# Завдання 1
# Створіть клас, який описує книгу. Він повинен містити інформацію про автора, назву, рік видання та жанр.
# Створіть кілька різних книжок. Визначте для нього методи repr та str.


class Book:
    def __init__(self, autor, title, year, genre):
        self.autor = autor
        self.title = title
        self.year = year
        self.genre = genre

    def __str__(self):
        return (
            f"The book with the title '{self.title}' written by {self.autor} was published in {self.year}, "
            f"the genre is a {self.genre.lower()}"
        )

    def __repr__(self):
        return f"Book = '{self.title}', Author = {self.autor}, Year = {self.year}, Genre = {self.genre}"


book1 = Book("Charles Dickens", "Great Expectations", "1861", "Novel")
book2 = Book("William Shakespeare", "Hamlet", "1603", "Tragedy")
print(repr(book1))
print(str(book2))
