# Створіть клас, який описує автомобіль. Створіть клас автосалону, що містить в собі список автомобілів,
# доступних для продажу, і функцію продажу заданого автомобіля


class Car:
    def __init__(self, make, model, year, price):
        self.make = make
        self.model = model
        self.year = year
        self.price = price

    def __str__(self):
        return f"{self.make} {self.model}, {self.year}, ${self.price}"


class CarShowroom:
    def __init__(self):
        self.available_cars = []

    def add_car(self, car):
        self.available_cars.append(car)

    def show_available_cars(self):
        return self.available_cars

    def __str__(self):
        cars_list = "\n".join([str(car) for car in self.available_cars])
        return f"Available cars in the showroom:\n{cars_list}"


car1 = Car("Mercedes-Benz", "E-Class", 2017, 52000)
car2 = Car("BMW", "5-Series", 2019, 53000)
car3 = Car("Toyota", "Camry", 2018, 23000)

showroom = CarShowroom()

showroom.add_car(car1)
showroom.add_car(car2)
showroom.add_car(car3)

print(showroom)
