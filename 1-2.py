# Створіть клас, який описує відгук до книги. Додайте до класу книги поле – список відгуків.
# Зробіть так, щоб при виведенні книги на екран за допомогою функції print також виводилися відгуки до неї.


class BookReview:
    def __init__(self, reviewer, text):
        self.reviewer = reviewer
        self.text = text

    def __str__(self):
        return f"Review: {self.text}"


class Book:
    def __init__(self, author, title):
        self.author = author
        self.title = title
        self.reviews = []

    def show_review(self, review):
        self.reviews.append(review)

    def __str__(self):
        book_info = f"'{self.title}' by {self.author}. "
        if self.reviews:
            book_info += "Community reviews:\n"
            for review in self.reviews:
                book_info += f" {review}\n"
        else:
            book_info += "No reviews yet.\n"
        return book_info


book1 = Book("Sara Gruen", "Water for Elephants")
book2 = Book("Jane Austen", "Pride and Prejudice")

review1_1 = BookReview(book1.title, "Oh man, this was lovely.")
review1_2 = BookReview(book1.title, "This book was PERFECT and I loved it.")
review2_1 = BookReview(book2.title, "I finally did it!!!! And I loved it!!!")
review2_2 = BookReview(book2.title, "Always an utter delight.")

book1.show_review(review1_1)
book1.show_review(review1_2)
book2.show_review(review2_1)
book2.show_review(review2_2)

print(book1)
print(book2)
